#!/usr/bin/env python

from Adafruit_ADS1x15 import ADS1x15
import paho.mqtt.client as mqtt
import sched, time
import datetime
import json
import ssl

ambientTempPath = "/sys/bus/w1/devices/28-00043b4451ff/w1_slave"
hwcTempPath = "/sys/bus/w1/devices/28-00043e59ffff/w1_slave"

adcMultiplier=15.7
mqttChannel="sensors/value"
oneK=1000

mqttBrokerHost="<HOSTNAME>"
mqttBrokerPort=1883
mqttc = mqtt.Client("python_pub")

ca="/home/pi/Development/certs/ca.crt"
clientCert="/home/pi/Development/certs/client.crt"
clientKey="/home/pi/Development/certs/clientNoPass.key"
mqttc.tls_set(ca, certfile=clientCert, keyfile=clientKey, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1)

mqttc.connect(mqttBrokerHost, mqttBrokerPort)

s = sched.scheduler(time.time, time.sleep)

def getTemp(source):
	tfile = open(source) 
	text = tfile.read() 
	tfile.close() 
	secondline = text.split("\n")[1] 
	temperaturedata = secondline.split(" ")[9] 
	temperature = float(temperaturedata[2:]) 
	temperature = temperature / oneK 
	return temperature

def measureLoop(sc): 
	pga = 6144
	sps = 8
	adc = ADS1x15(ic=0x01)

	volts = (adc.readADCSingleEnded(0, pga, sps)/oneK)*adcMultiplier
	amps = (adc.readADCSingleEnded(1, pga, sps)/oneK)*adcMultiplier
	watts=volts*amps

	ts = time.time()
	dt = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%dT%H:%M:%S')
	
	ambientTemp = getTemp(ambientTempPath)
	hwcTemp = getTemp(hwcTempPath)
	
	payload = {
		 "DateTime":dt
		,"Volts":round(volts,3)
		,"Amps":round(amps,3)
		,"Watts":round(watts,3)
		,"AmbientTempDegC":round(ambientTemp,3)
		,"HWCTempDegC":round(hwcTemp,3)
	}
        #print "Sending:{}".format(json.dumps(payload))

	mqttc.publish(mqttChannel,json.dumps(payload)) 
	mqttc.loop(2) #timeout = 2s

    	sc.enter(30, 1, measureLoop, (sc,))

s.enter(1, 1, measureLoop, (s,))
s.run()

